## Installation:
1. `npm install` or `yarn`.

## Usage:
1. Open command prompt.
2. run `start-server <port>` where <port> is the desired port.

More information [here](https://github.com/typicode/json-server).